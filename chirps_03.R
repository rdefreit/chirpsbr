
# Packages ----------------------------------------------------------------

library(tidyverse)
library(rgee)
library(sf)
library(lubridate)
library(arrow)
library(tictoc)
ee_Initialize()

loop_number <- 3


# Geometries --------------------------------------------------------------

# Read and save Brazilian municipalities geometries
# brmun <- geobr::read_municipality() %>%
#   rename(geometry = geom)
# saveRDS(object = brmun, file = "brmun.rds", compress = "xz")

# Read geometries file and create chunks of municipalities
brmun <- readRDS(file = "brmun.rds") %>%
  mutate(chunk = (seq(nrow(.))-1) %/% 100 + 1)

# Creates a list of chunks of municipalities
mun_chunks <- brmun %>%
  group_split(chunk)
rm(brmun)


# Dates -------------------------------------------------------------------

# The ECMWF/ERA5/DAILY data covers from 1979-01-02 to 2020-07-09  
dates <- tibble(
  date = seq(ymd("2011-01-01"), ymd("2021-12-31"), by = "day")
) %>%
  mutate(chunk = (seq(nrow(.))-1) %/% 365 + 1)


# Loop config and recovery ------------------------------------------------

dates <- dates[dates$chunk == loop_number,]$date

if(file.exists(paste0("d_", str_pad(loop_number, 2, pad = "0"), ".rds"))){
  last_date <- readRDS(file = paste0("d_", str_pad(loop_number, 2, pad = "0"), ".rds"))
  message(paste("Last date saved:", last_date))
  dates <- dates[dates > last_date]
}

# Loop --------------------------------------------------------------------

for(d in dates){
  d <- as.Date(d, origin = "1970-01-01")
  message(paste("Date", as.Date(d, origin = "1970-01-01")))
  
  tmp <- tibble()
  
  for(c in mun_chunks){
    tic()
    message(paste("Chunk", c$chunk[1]))
    
    # Prepare precipitation data collection
    chirps_prec <- ee$ImageCollection("UCSB-CHG/CHIRPS/DAILY") %>%
      ee$ImageCollection$filterDate(as.character(d)) %>%
      ee$ImageCollection$map(function(x) x$select("precipitation")) %>%
      ee$ImageCollection$toBands() %>% # 
      ee$Image$rename("value")
    
    # Extract precipitation average
    prec_average <- ee_extract(
      x = chirps_prec, 
      y = c["code_muni"], 
      sf = FALSE,
      fun = ee$Reducer$mean(),
      quiet = TRUE
    )
    
    # Extract precipitation sdtdev
    prec_stddev <- ee_extract(
      x = chirps_prec, 
      y = c["code_muni"], 
      sf = FALSE,
      fun = ee$Reducer$stdDev(),
      quiet = TRUE
    )
    
    # Extract precipitation max
    prec_max <- ee_extract(
      x = chirps_prec, 
      y = c["code_muni"], 
      sf = FALSE,
      fun = ee$Reducer$max(),
      quiet = TRUE
    )
    
    # Extract precipitation min
    prec_min <- ee_extract(
      x = chirps_prec, 
      y = c["code_muni"], 
      sf = FALSE,
      fun = ee$Reducer$min(),
      quiet = TRUE
    )
    
    # Extract precipitation cells count
    prec_count <- ee_extract(
      x = chirps_prec, 
      y = c["code_muni"], 
      sf = FALSE,
      fun = ee$Reducer$count(),
      quiet = TRUE
    )
    
    # Extract precipitation sum
    prec_sum <- ee_extract(
      x = chirps_prec, 
      y = c["code_muni"], 
      sf = FALSE,
      fun = ee$Reducer$sum(),
      quiet = TRUE
    )
    
    # Store current date
    prec_average$date <- d
    prec_stddev$date <- d
    prec_count$date <- d
    prec_sum$date <- d
    
    # Prepare data structure
    prec_average <- prec_average %>%
      mutate(name = "avg")
    prec_stddev <- prec_stddev %>%
      mutate(name = "sd")
    prec_max <- prec_max %>%
      mutate(name = "max")
    prec_min <- prec_min %>%
      mutate(name = "min")
    prec_count <- prec_count %>%
      mutate(name = "count")
    prec_sum <- prec_sum %>%
      mutate(name = "sum")
    
    # Append
    tmp <- bind_rows(tmp, prec_average, prec_stddev, 
                     prec_max, prec_min, prec_count, 
                     prec_sum)
    
    toc()
  }
  # Save data from current date
  write_parquet(
    x = tmp, 
    sink = paste0(
      "parquets/chirpsbr_", 
      str_pad(loop_number, 2, pad = "0"), 
      "_",
      format(d, "%Y%m%d"), 
      ".parquet"
    )
  )
  saveRDS(object = d, file = paste0("d_", str_pad(loop_number, 2, pad = "0"), ".rds"))
  
  message("Done!")
}
