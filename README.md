# chirpsbr

**Objective**. This project aims to high-quality precipitation data aggregated by Brazilian municipalities and dates.

**Data source**: CHIRPS Daily, Version 2.0 Final. Available at [Google Earth Engine](https://developers.google.com/earth-engine/datasets/catalog/UCSB-CHG_CHIRPS_DAILY?hl=en).

**Final products**: Parquet files with precipitation indicators, aggregated by Brazilian municipalities and dates. From 2011-01-01 to 2021-01-01. Average, standard deviation, cell counts, and sum are provided.
